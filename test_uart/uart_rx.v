`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:21:00 11/01/2016 
// Design Name: 
// Module Name:    uart_rx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module uart_rx(
    input wire clk,
    input wire rst,
    input wire rx,
    output  [7:0] data,
    output reg ready
//	 output reg half_tick,
//	 output reg tick
    );
	 
// signals to the instantiation
reg sig_start;


// states
parameter [3:0]
	IDLE = 4'b0000, W_HALF_TICK = 4'b0001, S_B_0 = 4'b0010, S_B_1 = 4'b0011,
	S_B_2 = 4'b0100, S_B_3 = 4'b0101, S_B_4 = 4'b0110, S_B_5 = 4'b0111,
	S_B_6 = 4'b1000, S_B_7 = 4'b1001, S_B_STOP = 4'b1010;

reg[3:0] state_a, state_f;

reg [7:0] regdata_a;
reg [7:0] regdata_f;

// constants
parameter B_START = 0;
parameter B_STOP = 1;

// instantiaton
counter #(5208) counter_rate (
    .clk(clk), 
    .rst(sig_start), 
    .half_tc(sig_half_tick), 
    .tc(sig_tick)
    );
	 
assign data = regdata_a;	 

// memory
always @ (posedge clk) begin : MEMORY
	if (rst == 0) begin
		state_a <= IDLE;
		regdata_a <= { 8{1'b0} };
//		half_tick <= 1'b0;
//		tick <= 1'b0;
	end else begin
		state_a <= state_f;
		regdata_a <= regdata_f;
//		half_tick <= sig_half_tick;
//		tick <= sig_tick;
	end
end		 
	 
// next state logic
always @(state_a or rx or sig_half_tick or sig_tick) begin : NEX_STATE_LOGIC
	state_f <= IDLE;
	case (state_a)
		IDLE : if (rx == B_START) begin
					state_f <= W_HALF_TICK;
				 end else begin
					state_f <= IDLE;
				 end
		W_HALF_TICK : if (sig_half_tick) begin
							state_f <= S_B_0;
						end else begin
							state_f <= W_HALF_TICK;
						end
		S_B_0 : if (sig_tick) begin
						state_f <= S_B_1;
					end else begin
						state_f <= S_B_0;
					end
		S_B_1 : if (sig_tick) begin
						state_f <= S_B_2;
					end else begin
						state_f <= S_B_1;
					end
		S_B_2 : if (sig_tick) begin
						state_f <= S_B_3;
					end else begin
						state_f <= S_B_2;
					end
		S_B_3 : if (sig_tick) begin
						state_f <= S_B_4;
					end else begin
						state_f <= S_B_3;
					end
		S_B_4 : if (sig_tick) begin
						state_f <= S_B_5;
					end else begin
						state_f <= S_B_4;
					end
		S_B_5 : if (sig_tick) begin
						state_f <= S_B_6;
					end else begin
						state_f <= S_B_5;
					end
		S_B_6 : if (sig_tick) begin
						state_f <= S_B_7;
					end else begin
						state_f <= S_B_6;
					end
		S_B_7 : if (sig_tick) begin
						state_f <= S_B_STOP;
					end else begin
						state_f <= S_B_7;
					end
		S_B_STOP : if (sig_tick) begin
						state_f <= IDLE;
					end else begin
						state_f <= S_B_STOP;
					end
	endcase
end

// output logic
always @ (state_a or rx or sig_tick or sig_half_tick or regdata_a) begin : OUTPUT_LOGIC
	regdata_f <= regdata_a;
	sig_start <= 1'b0;
	ready <= 1'b0;
	case (state_a)
		IDLE : if (rx == B_START) begin
					regdata_f <= { 8{1'b0} };
					sig_start <= 1'b1;
				end
		W_HALF_TICK : if (sig_half_tick) begin
							sig_start <= 1'b1;
						end
		S_B_0 : if (sig_tick) begin
						regdata_f[0] <= rx;
					end else begin
						regdata_f[0] <= regdata_a[0];
					end
		S_B_1 : if (sig_tick) begin
						regdata_f[1] <= rx;
					end else begin
						regdata_f[1] <= regdata_a[1];
					end
		S_B_2 : if (sig_tick) begin
						regdata_f[2] <= rx;
					end else begin
						regdata_f[2] <= regdata_a[2];
					end 
		S_B_3 : if (sig_tick) begin
						regdata_f[3] <= rx;
					end else begin
						regdata_f[3] <= regdata_a[3];
					end 
		S_B_4 : if (sig_tick) begin
						regdata_f[4] <= rx;
					end else begin
						regdata_f[4] <= regdata_a[4];
					end 
		S_B_5 : if (sig_tick) begin
						regdata_f[5] <= rx;
					end else begin
						regdata_f[5] <= regdata_a[5];
					end 
		S_B_6 : if (sig_tick) begin
						regdata_f[6] <= rx;
					end else begin
						regdata_f[6] <= regdata_a[6];
					end 
		S_B_7 : if (sig_tick) begin
						regdata_f[7] <= rx;
					end else begin
						regdata_f[7] <= regdata_a[7];
					end 
		S_B_STOP : if (sig_tick) begin
							ready <= 1'b1;
						end
	endcase
end

endmodule
