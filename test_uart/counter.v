`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:34:49 10/30/2016 
// Design Name: 
// Module Name:    counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module counter #(
	 parameter M = 10
	 )(
	 input wire clk,
    input wire rst,
    output reg half_tc,
    output reg tc
    );


function integer my_log2;
    input [31:0] depth;
	 integer i;
    begin
         i = depth;        
        for(my_log2 = 0; i > 0; my_log2 = my_log2 + 1)
            i = i >> 1;
    end
endfunction


localparam integer N = my_log2(M);

reg [N-1:0] count_a;
reg [N-1:0] count_f;

// MEMORY
always @ (posedge clk) begin : MEMORY
	if (rst == 1'b1) begin
		count_a <=  { N-1 {1'b0} };
	end else begin
		count_a <= count_f;
	end
end
	
// NEXT STATE LOGIC
always @ (rst, count_a) begin : NEX_STATE_LOGIC
	if (rst == 1'b1) begin
		count_f <= { N-1 {1'b0} };
	end else if (count_a == M) begin
		count_f <= { N-1 {1'b0} };
	end else begin
		count_f <= count_a + 1;
	end
end

// OUTPUT LOGIC
always @ (count_a) begin : OUTPUT_LOGIC
	if (count_a == M) begin					// Debería ser (N-1) ' b (M-1) pero no le gusta y puse 5 en vez del (M-1)
		tc <= 1'b1;
	end else begin
		tc <= 1'b0;
	end
	
	if (count_a == (M/2)) begin
		half_tc <= 1'b1;
	end else begin
		half_tc <= 1'b0;
	end
end


endmodule

