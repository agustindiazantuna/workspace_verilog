`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   01:14:16 11/02/2016
// Design Name:   uart_rx
// Module Name:   /home/agustin/Dropbox/TD1_A_2015/Verilog/test_uart/uart_rx_tb.v
// Project Name:  test_uart
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: uart_rx
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module uart_rx_tb;

	// Inputs
	reg clk;
	reg rst;
	reg rx;

	// Outputs
	wire [7:0] data;
	wire ready;

	// Instantiate the Unit Under Test (UUT)
	uart_rx uut (
		.clk(clk), 
		.rst(rst), 
		.rx(rx), 
		.data(data), 
		.ready(ready)
	);

	// Clock funcionando a fclk = 2.4576 MHz -> T = 1/fclk = 407 ns 
	always 
		#203  clk =  ! clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;			// reset module
		rx = 1;			// idle
		
		// Add stimulus here
		#104000
		rst = 1;			// enable module
		#104000
		rx = 0;			// bit start
		#104000
		rx = 1;			// b0
		#104000
		rx = 0;			// b1
		#104000
		rx = 1;			// b2
		#104000
		rst = 0;			// reset module
		rx = 1;			// idle
		#104000
		rx = 0;			// bit start with reset ON
		#104000
		rx = 1;			// b0
		#104000
		rx = 0;			// b1
		#1218
		rx = 0;			// bad time
		#812
		rx = 1;			// idle
		#104000
		rst = 1;			// enable module
		#104000
		rx = 0;			// bit start
		#104000
		rx = 1;			// b0
		#104000
		rx = 0;			// b1
		#104000
		rx = 1;			// b2
		#104000
		rx = 0;			// b3
		#104000
		rx = 1;			// b4
		#104000
		rx = 0;			// b5
		#104000
		rx = 1;			// b6
		#104000
		rx = 0;			// b7
		#104000
		rx = 1;			// bit stop

		#105000 $finish;

	end
      
endmodule

