`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:42:02 11/01/2016
// Design Name:   uart_tx
// Module Name:   /home/agustin/Dropbox/TD1_A_2015/Verilog/test_uart/uart_tx_tb.v
// Project Name:  test_uart
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: uart_tx
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module uart_tx_tb;

	// Inputs
	reg clk;
	reg rst;
	reg send;
	reg [7:0] data;

	// Outputs
	wire busy;
	wire tx;

	// Instantiate the Unit Under Test (UUT)
	uart_tx uut (
		.clk(clk), 
		.rst(rst), 
		.send(send), 
		.data(data), 
		.busy(busy), 
		.tx(tx)
	);
	
	// Clock funcionando a fclk = 2.4576 MHz -> T = 1/fclk = 407 ns 
	always 
		#203  clk =  ! clk;
		
	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;						// reset module
		send = 0;
		data = 0;
		#1218
		
		// Add stimulus here
		rst = 1;						// enable module
		#406
		send = 1;					// data ready to send
		data = 8'b10101010;		// data
		#406
		send = 0;
		#520000						// 5 tx
		rst = 0;						// reset module
		#406
		send = 1;					// data ready to send with reset ON
		data = 8'b11111010;		// data
		#406
		send = 0;
		#208000						// 2 tx
		rst = 1;						// enable module
		#208000						// 2 tx
		send = 1;					// data ready to send
		data = 8'b11111010;		// data
		#406
		send = 0;
		#208000
		send = 1'b1;				// data ready to send while tx data
		data = 8'b11111010;		// data
		#406
		send = 0;
		#1042000
		send = 1'b1;				// new data ready to send
		data = 8'b11111010;		// data
		#406
		send = 0;
		#1042000 $finish;
	end
      
endmodule

