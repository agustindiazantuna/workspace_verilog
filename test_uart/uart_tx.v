`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:26:29 10/31/2016 
// Design Name: 
// Module Name:    uart_tx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module uart_tx(
    input wire clk,
    input wire rst,
    input wire send,
    input wire [7:0] data,
    output reg busy,
	 output reg tx
    );

// signals to the instantiation
reg sig_start;
//wire sig_tick;

// states
parameter [3:0]
	IDLE = 4'b0000, S_B_START = 4'b0001, S_B_0 = 4'b0010, S_B_1 = 4'b0011,
	S_B_2 = 4'b0100, S_B_3 = 4'b0101, S_B_4 = 4'b0110, S_B_5 = 4'b0111,
	S_B_6 = 4'b1000, S_B_7 = 4'b1001, S_B_STOP = 4'b1010;

reg[3:0] state_a, state_f;

reg [7:0] regdata_a;
reg [7:0] regdata_f;

// constants
parameter B_IDLE = 1;
parameter B_START = 0;
parameter B_STOP = 1;

// instantiaton
counter #(5208) counter_rate (
    .clk(clk), 
    .rst(sig_start), 
    .half_tc(open), 
    .tc(sig_tick)
    );

// memory
always @ (posedge clk) begin : MEMORY
//	regdata_a <= { 7 {1'b0} };
	if (rst == 0) begin
		state_a <= IDLE;
		regdata_a <= {7{1'b1}};
	end else begin
		state_a <= state_f;
		regdata_a <= regdata_f;
	end
end	

// next state logic
always @(state_a or send or sig_tick or data or regdata_a) begin : NEX_STATE_LOGIC
	regdata_f <= regdata_a;
	state_f <= S_B_START;
	case (state_a)
		IDLE : if (send) begin
					regdata_f <= data;
					state_f <= S_B_START;
				 end else begin
					state_f <= IDLE;
				 end
		S_B_START :	if (sig_tick) begin
							state_f <= S_B_0;
						end else begin
							state_f <= S_B_START;
						end
		S_B_0 : if (sig_tick) begin
						state_f <= S_B_1;
					end else begin
						state_f <= S_B_0;
					end
		S_B_1 : if (sig_tick) begin
						state_f <= S_B_2;
					end else begin
						state_f <= S_B_1;
					end
		S_B_2 : if (sig_tick) begin
						state_f <= S_B_3;
					end else begin
						state_f <= S_B_2;
					end
		S_B_3 : if (sig_tick) begin
						state_f <= S_B_4;
					end else begin
						state_f <= S_B_3;
					end
		S_B_4 : if (sig_tick) begin
						state_f <= S_B_5;
					end else begin
						state_f <= S_B_4;
					end
		S_B_5 : if (sig_tick) begin
						state_f <= S_B_6;
					end else begin
						state_f <= S_B_5;
					end
		S_B_6 : if (sig_tick) begin
						state_f <= S_B_7;
					end else begin
						state_f <= S_B_6;
					end
		S_B_7 : if (sig_tick) begin
						state_f <= S_B_STOP;
					end else begin
						state_f <= S_B_7;
					end
		S_B_STOP : if (sig_tick) begin
						state_f <= IDLE;
					end else begin
						state_f <= S_B_STOP;
					end
	endcase
end

// output logic
always @ (state_a or send or sig_tick or regdata_a) begin : OUTPUT_LOGIC
	sig_start <= 1'b0;
	busy <= 1'b1;
	tx <= 1'b1;
	case (state_a)
		IDLE : if (send) begin
					sig_start <= 1'b1;
					tx <= B_START;
				end else begin
					busy <= 1'b0;
					tx <= 1'b1;
				end
		S_B_START : if (sig_tick) begin
							tx <= regdata_a[0];
						end else begin
							tx <= B_START;
						end
		S_B_0 : if (sig_tick) begin
						tx <= regdata_a[1];
					end else begin
						tx <= regdata_a[0];
					end
		S_B_1 : if (sig_tick) begin
						tx <= regdata_a[2];
					end else begin
						tx <= regdata_a[1];
					end
		S_B_2 : if (sig_tick) begin
						tx <= regdata_a[3];
					end else begin
						tx <= regdata_a[2];
					end 
		S_B_3 : if (sig_tick) begin
						tx <= regdata_a[4];
					end else begin
						tx <= regdata_a[3];
					end
		S_B_4 : if (sig_tick) begin
						tx <= regdata_a[5];
					end else begin
						tx <= regdata_a[4];
					end
		S_B_5 : if (sig_tick) begin
						tx <= regdata_a[6];
					end else begin
						tx <= regdata_a[5];
					end
		S_B_6 : if (sig_tick) begin
						tx <= regdata_a[7];
					end else begin
						tx <= regdata_a[6];
					end
		S_B_7 : if (sig_tick) begin
						tx <= B_STOP;
					end else begin
						tx <= regdata_a[7];
					end
		S_B_STOP : if (sig_tick) begin
							tx <= B_IDLE;
							busy <= 1'b0;
						end else begin
							tx <= B_STOP;
						end
	endcase
end

endmodule


