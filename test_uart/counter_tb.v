`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:33:12 10/30/2016
// Design Name:   counter
// Module Name:   /home/agustin/Dropbox/TD1_A_2015/Verilog/test_uart/counter_tb.v
// Project Name:  test_uart
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: counter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module counter_tb;

	// Inputs
	reg clk;
	reg rst;

	// Outputs
	wire half_tc;
	wire tc;

	// Instantiate the Unit Under Test (UUT)
	counter uut (
		.clk(clk), 
		.rst(rst), 
		.half_tc(half_tc), 
		.tc(tc)
	);
	
	always 
		#5  clk =  ! clk; 
		
	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;

		// Wait 100 ns for global reset to finish
		#15;
      
		// Add stimulus here
		rst = 0;
		
		#300 $finish;

	end 

endmodule

