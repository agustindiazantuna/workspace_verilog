`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:07:27 11/09/2016 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
    input wire clk,
	 input wire rst,
    input wire rx,
    output reg tx
//	 output reg [7:0] leds,
//	 output reg tick,
//	 output reg half_tick
    );
	 
	 
// signals to the instantiation
wire [7:0] sig_data;
wire sig_ready_send;
wire sig_tx;

// Instantiate the module
uart_rx instance_uart_rx (
    .clk(clk), 
    .rst(rst), 
    .rx(rx), 
    .data(sig_data), 
    .ready(sig_ready_send)
//	 .half_tick(sig_half_tick),
//	 .tick(sig_tick)
    );

// Instantiate the module
uart_tx instance_uart_tx (
    .clk(clk), 
    .rst(rst), 
    .send(sig_ready_send), 
    .data(sig_data), 
    .busy(open), 
    .tx(sig_tx)
    );

	 
always @ (posedge clk) begin : salida
	tx <= sig_tx;
//	leds <= sig_data;
//	half_tick <= sig_half_tick;
//	tick <= sig_tick;
end

endmodule
