
module mux4to1 #(
	parameter WIDTH= 8
	) (
	input [WIDTH-1:0] din1, din2, din3, din4,
	input [1:0] select,
	output [WIDTH-1:0] dout					// preguntar si podemos cambiar a reg
	);
	
	reg [WIDTH-1:0] dout_reg;
	
	assign dout = dout_reg;					// si se puede hacer lo de arriba evitamos esto
	
	always@( din1, din2, din3, din4, select )
	begin
		case ( select )
			2'd0 : dout_reg = din1;
			2'd1 : dout_reg = din2;
			2'd2 : dout_reg = din3;
			default : dout_reg = din4;
		endcase
	end

endmodule
