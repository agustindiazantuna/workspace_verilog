
module alu #(
	parameter WIDTH = 8
	) (
	input [WIDTH-1:0] in1, in2,
	input [3:0] op,				// 1 add - 2 sub - 4 mul - 8 div
	input [0:0] nvalid_data,
	output reg [2*WIDTH-1:0] out,		// idem a los out de
	output reg [0:0] zero,				// bloques procedurales
	output reg [0:0] error				// anteriores
	);

	localparam	SUM = 4'b0001;
	localparam	RES = 4'b0010;
	localparam	MUL = 4'b0100;
	localparam	DIV = 4'b1000;

	reg [2*WIDTH-1:0] out_reg;

	always@( in1, in2, op, nvalid_data )
	begin
		if ( nvalid_data | (( op == DIV ) && ( 0 == |in2 ))) // dato invalido o division por cero
		begin		
			error = 1'd1;
			out_reg = {(2*WIDTH){1'd1}};
		end
		else
		begin
			error = 1'd0;
			case ( op )
				SUM : out_reg = in1 + in2;			// tengo que extender el signo
				RES : out_reg = in1 - in2;			// o verilog entiende eso
				MUL : out_reg = in1 * in2;
				DIV : out_reg = in1 / in2;
				default :
					begin 
						error = 1'd1;
						out_reg = {(2*WIDTH){1'd1}};
					end
			endcase
		end
		
		zero = ~|out_reg;
		out = out_reg;
	end

endmodule
