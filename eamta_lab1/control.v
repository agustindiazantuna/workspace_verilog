module control (
	input wire [0:0] clk,
	input wire [0:0] rst,
	input wire [5:0] cmd_in,
	input wire [0:0] p_error,
	output reg [0:0] aluin_reg_en,
	output reg [0:0] datain_reg_en,
	output reg [0:0] aluout_reg_en,
	output reg [0:0] nvalid_data,
	output wire [1:0] in_select_a,
	output wire [1:0] in_select_b,
	output reg [3:0] opcode
	);
	
	// variables
	localparam [2:0] LOAD_OP = 3'b001, OPER = 3'b010, OUTPUT = 3'b100;
	reg [2:0] state = LOAD_OP, next = OPER;
	reg [3:0] opcode_next = SUM;
	localparam	SUM = 4'b0001;
	localparam	RES = 4'b0010;
	localparam	MUL = 4'b0100;
	localparam	DIV = 4'b1000;
	
	// memoria
	always @(posedge clk) begin
		if (rst)	begin 
			state <= LOAD_OP;
			opcode <= SUM;
		end
		else begin
			state <= next;
			opcode <= opcode_next;
		end
	end
	
	assign in_select_a = cmd_in[5:4];
	assign in_select_b = cmd_in[3:2];
	
	// logica de estado futuro
	always @(*) begin
		// asignaciones por defecto
		datain_reg_en = 1'b0;	
		aluin_reg_en = 1'b0;
		aluout_reg_en = 1'b0;
		nvalid_data = 1'b0;		// ver salida porque como lo hicimos no tiene uso
		opcode_next = opcode;
	
		case (state)
			LOAD_OP:
			begin
				datain_reg_en = 1'b1;	// enable reg data in
				case ( cmd_in[1:0] )
					2'd0 : opcode_next = SUM;
					2'd1 : opcode_next = RES;
					2'd2 : opcode_next = MUL;
					2'd3 : opcode_next = DIV;
				default : nvalid_data = 1'b1;
				endcase
				next = OPER;
			end
			OPER:
			begin
				if(~(p_error && (&cmd_in[5:4] || &cmd_in[3:2])))
// ~(la condicion es true si se pide realimentar una salida erronea)
				begin
					aluin_reg_en = 1'b1;	// en muxA muxB
					nvalid_data = 1'b0;
				end
//				else	nvalid_data = 1'b1;
				next = OUTPUT;
			end
			OUTPUT:
			begin
				if(~(p_error && (&cmd_in[5:4] || &cmd_in[3:2])))
					aluout_reg_en = 1'b1;	// en muxA muxB
				next = LOAD_OP;
			end
			default:
			begin
				nvalid_data = 1'b1;
				next = LOAD_OP;
			end
		endcase
	end

endmodule
