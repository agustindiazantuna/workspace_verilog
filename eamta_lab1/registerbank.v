
module registerbank #(
	parameter WIDTH = 8
	) (
	input [0:0] clk,
	input [0:0] rst,
	input [0:0] wr_en,
	input [WIDTH-1:0] in,
	output reg [WIDTH-1:0] out		// idem al reg anterior
	);

	always @(posedge clk)
	begin
		if (rst)
			out <= 'd0; // si no hay que hacer {(WIDTH){1'd0}}
		else if (wr_en)
			out <= in;
	end

endmodule
