module top #(
	parameter WIDTH = 8 
	)(
	input wire [0:0] clk,
	input wire [0:0] rst,
	input wire [5:0] cmdin,
	input wire [WIDTH-1:0] din_1,
	input wire [WIDTH-1:0] din_2,
	input wire [WIDTH-1:0] din_3,
	output wire [WIDTH-1:0] dout_low,
	output wire [WIDTH-1:0] dout_high,
	output wire [0:0] zero,
	output wire [0:0] error,
);

	// signals

	// Instantiate the module control
	control #(
		.WIDTH(WIDTH)
		) inst_control (
		.clk(clk), // faltan todos
		.rst(rst), 
		.cmd_in(cmd_in), 
		.p_error(p_error), 
		.aluin_reg_en(aluin_reg_en), 
		.datain_reg_en(datain_reg_en), 
		.aluout_reg_en(aluout_reg_en), 
		.nvalid_data(nvalid_data), 
		.in_select_a(in_select_a), 
		.in_select_b(in_select_b), 
		.opcode(opcode)
		);

	// Instantiate the module alu
	alu #(
		.WIDTH(WIDTH)
		) inst_alu (
		.in1(in1), // faltan todos
		.in2(in2), 
		.op(op), 
		.nvalid_data(nvalid_data), 
		.out(out), 
		.zero(zero), 
		.error(error)
		);

	// Instantiate the module muxA and muxB
	mux4to1 #(
		.WIDTH(WIDTH)
		) inst_muxA (
		.din1(din_1), 
		.din2(din_2), 
		.din3(din_3), 
		.din4(din4), 	//Asdf
		.select(select), //ASDf
		.dout(dout)		//Adsf
		);

	mux4to1 #(
		.WIDTH(WIDTH)
		) inst_muxB (
		.din1(din_1), 
		.din2(din_2), 
		.din3(din_3), 
		.din4(din4), 	//Asdf
		.select(select), //ASDf
		.dout(dout)		//Adsf
		);	 

	// Instantiate the module register
	registerbank #(
		.WIDTH(WIDTH*2)
		) inst_aluin_reg (
		.clk(clk), 
		.rst(rst), 
		.wr_en(wr_en), //ASdf
		.in(in), 	// adfs
		.out(out)	//asdf
		);

	registerbank #(
		.WIDTH((WIDTH*2)+2)
		) inst_aluout_reg (
		.clk(clk), 
		.rst(rst), 
		.wr_en(wr_en), 		//asdfas
		.in(in), 				//ASdfa
		.out(out)				//ASfd
		);

	registerbank #(
		.WIDTH(6)
		) inst_cmdin_reg (
		.clk(clk), 
		.rst(rst), 
		.wr_en(wr_en), 
		.in(cmdin), 
		.out(out)				// asdfasd
		);
		
endmodule
