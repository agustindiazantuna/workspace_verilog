//////////////////////////////////////////////////////////////////////////////////
// UNIDAD DE CONTROL (CONTROL)
//
//////////////////////////////////////////////////////////////////////////////////

module control(
    input wire [7:0] data_rom,   // FROM ROM
    input wire [3:0] flags,      // FROM ALU_ACCUMULATOR (to decoder)
    input wire [7:0] acc,        // FROM ALU_ACCUMULATOR
    input wire rst,
    input wire clk,
    output reg [1:0] sel_acc,    // ALU_ACCUMULATOR
    output reg [7:0] i,          // ROM INSTANTANEO
    output reg nr_w,             // RAM
    input wire [7:0] din_ram,    // RAM
    output reg [7:0] din_ram_reg,// RAM
    output wire [7:0] dout_ram,  // RAM
    output wire [7:0] add_ram,   // RAM
    output reg [1:0] en_pc,      // PC
    output wire [2:0] opcode,    // ALU_ACCUMULATOR (from decoder)
    output wire sel_b,           // ALU_ACCUMULATOR (from decoder)
    output reg sel_flags         // ALU_ACCUMULATOR (from decoder)
    );
    
    // estados
    localparam FETCH = 2'b00;
    localparam DECODE = 2'b01;
    localparam MEMORY = 2'b10;
    localparam EXCECUTE = 2'b11;
    
    // signals registros
    reg [1:0] state_reg;
    reg [1:0] state_next;
    reg [7:0] inst_reg;    // instruccion de la rom
    reg [7:0] inst_next;   // instruccion de la rom
    reg [7:0] data_reg;    // dato instantaneo de la rom
    reg [7:0] data_next;   // dato instantaneo de la rom
    reg [7:0] data_ram_reg;    // dato instantaneo de la ram
    reg [7:0] data_ram_next;   // dato instantaneo de la ram

    // signals decoder
    wire to_memory;
    wire sel_dout_ram;
    wire sel_add_ram;
    wire sel_rw;
    wire [1:0] sig_sel_acc;
    wire sig_en_pc;
    wire sig_sel_flags;
    
    // Instantiate the module DECO
    decoder inst_deco (
    .inst(inst_reg), 
    .flags(flags), 
    .to_memory(to_memory), 
    .sel_dout_ram(sel_dout_ram), 
    .sel_add_ram(sel_add_ram), 
    .sel_rw(sel_rw), 
	 .sel_flags(sig_sel_flags),
    .sel_b(sel_b), 
    .opcode(opcode), 
    .sel_acc(sig_sel_acc), 
    .en_pc(sig_en_pc)
    );
    
    // registos de estados y datos de la rom
    always@(posedge clk, posedge rst) begin
      if(rst) begin
         state_reg <= FETCH;
         inst_reg <= 8'h00;
         data_reg <= 8'h00;
         data_ram_reg <= 8'h00;
      end
      else begin
         state_reg <= state_next;
         inst_reg <= inst_next;
         data_reg <= data_next;
         data_ram_reg <= data_ram_next;
      end
   end
   
   // logica de estado futuro
   always@(*) begin
      // asigno salida
      i = data_reg;
      sel_acc = 2'b01;              // retengo dato del accumulador
      en_pc[0] = 1'b0;
      en_pc[1] = 1'b0;
      
      // registro data from ram
      din_ram_reg = data_ram_reg;
      data_ram_next = din_ram;

      // valores por defecto
      inst_next = inst_reg;
      data_next = data_reg;
      nr_w = 1'b0;
      sel_flags = 1'b0;
         
      case(state_reg)
         FETCH: begin
            inst_next = data_rom;   // retengo instruccion
            en_pc[0] = 1'b1;        // en pc
//            if(inst_reg[1:0] == 2'b00)
//               en_pc[1] = sig_en_pc;        // en pc
            en_pc[1] = 1'b0;
            state_next = DECODE;
         end
         DECODE: begin
            data_next = data_rom;   // retengo dato inmediato
            if(to_memory) begin
               state_next = MEMORY;
            end
            else begin
               state_next = EXCECUTE;
            end
         end
         MEMORY: begin
            nr_w = sel_rw;
            state_next = EXCECUTE;
         end
         default: begin
            sel_acc = sig_sel_acc;
            sel_flags = sig_sel_flags;
            en_pc[1] = sig_en_pc;
            en_pc[0] = 1'b1;        // en pc
            state_next = FETCH;
         end
      endcase
   end
   
   // asigno salidas
   assign dout_ram = sel_dout_ram ? data_reg : acc;
   assign add_ram = sel_add_ram ? acc : data_reg;
   
endmodule


