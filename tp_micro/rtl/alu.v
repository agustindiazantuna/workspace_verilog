//////////////////////////////////////////////////////////////////////////////////
// Alu 8 bits:
//
// opcode
//		000	ADD
//		001	SUB
//		010	ADDC
//		011	SUBC
//		100	NOR
//		101	NAND
//		110	XOR
//		111	XNOR
//
// flags
//		flags[0]		cout
//		flags[1]		zero
//		flags[2]		negative
//		flags[3]		ov
//
//////////////////////////////////////////////////////////////////////////////////

module alu(
    input wire [7:0] a,
    input wire [7:0] b,
    input wire cin,
    input wire [2:0] opcode,
    output reg [7:0] r,
    output reg [3:0] flags
    );

	// opcode logic
	localparam op_NOR = 3'b100;
	localparam op_NAND = 3'b101;
	localparam op_XOR = 3'b110;
	localparam op_XNOR = 3'b111;

	// signals
	wire sig_cin;
   reg [8:0] sig_r;
	
   // cin para resolver addc y subc
	assign sig_cin = opcode[1] ? cin : 1'b0;

   // mux para operaciones logicas
	always @(*) begin     
		flags[2] = 1'b0; // negative
		case(opcode)
			op_NOR: begin
				sig_r = ~(a|b);
			end
			op_NAND: begin
				sig_r = ~(a&b);
			end
			op_XOR: begin
				sig_r = (a^b);
			end
			op_XNOR: begin
				sig_r = ~(a^b);
			end
			default: begin
				if(!opcode[0])
					sig_r = a + b + sig_cin;
				else begin
					sig_r = a - b - sig_cin;
					flags[2] = sig_r[7]; // negative
				end
			end
		endcase
   end
   
   always@(*) begin
   // flags
      if(opcode[0])
         flags[3] = (a[7] & b[7] & ~sig_r[7]) | (~a[7] & ~b[7] & sig_r[7]); // ov add
      else
         flags[3] = (~a[7] & b[7] & sig_r[7]) | (a[7] & ~b[7] & ~sig_r[7]); // ov sub

      flags[0] = sig_r[8]; // cout
      flags[1] = ~|sig_r[7:0]; //zero

      // salida
      r = sig_r[7:0];
   end 

endmodule

