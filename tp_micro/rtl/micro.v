//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////

module micro(
    input [7:0] rom_data,
    input [7:0] ram_data_rd,
    input arst,
    input clk,
    output [7:0] ram_data_wr,
    output [7:0] ram_addr,
    output [7:0] rom_addr,
    output ram_wr_en
    );
    
	// signal rst
	wire srst;
	 
   // signals ALU_ACC
   wire [7:0] sig_i;
   wire sig_sel_b;
	wire sig_sel_flags;
   wire [1:0] sig_sel_acc;
   wire [1:0] sig_sel_acc_fdec;
   wire [2:0] sig_opcode;
   wire [3:0] sig_flags;
   wire [7:0] sig_acc;
   
   // signals CONTROL
   wire sig_to_memory;
   wire sig_sel_dout_ram;
   wire sig_sel_add_ram;
   wire sig_sel_read;
   wire [1:0] sig_en_pc;
   wire [7:0] sig_inst;  

   // signal ram_data_rd_reg
   wire [7:0] ram_data_rd_reg;
   
	// Instantiate the module SYNC
	sync instance_sync (
    .arst(arst), 
    .clk(clk), 
    .sync(srst)
    );
	
   // Instantiate the module ALU_ACC
   alu_accumulator inst_alu_acc (
    .x(ram_data_rd_reg), 
    .i(sig_i), 
    .sel_b(sig_sel_b), 
	 .sel_flags(sig_sel_flags),
    .sel_acc(sig_sel_acc), 
    .opcode(sig_opcode), 
    .rst(srst), 
    .clk(clk), 
    .flags(sig_flags), 
    .acc(sig_acc)
    );
    
    // Instantiate the module PC
    program_counter inst_pc (
    .i(sig_i), 
    .en_pc(sig_en_pc), 
    .rst(srst), 
    .clk(clk), 
    .add_rom(rom_addr)
    );
    
    // Instantiate the module CONTROL
    control inst_control (
    .data_rom(rom_data), 
    .flags(sig_flags), 
    .acc(sig_acc), 
    .rst(srst), 
    .clk(clk), 
    .sel_acc(sig_sel_acc), 
    .i(sig_i), 
    .nr_w(ram_wr_en), 
    .din_ram(ram_data_rd), 
    .din_ram_reg(ram_data_rd_reg), 
    .dout_ram(ram_data_wr), 
    .add_ram(ram_addr), 
    .en_pc(sig_en_pc), 
    .opcode(sig_opcode), 
    .sel_b(sig_sel_b), 
    .sel_flags(sig_sel_flags)
    );
    
    
endmodule
