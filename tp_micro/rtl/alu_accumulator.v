//////////////////////////////////////////////////////////////////////////////////
// Alu + accumulator
//
// sel_b
//   0     b <= x
//   1     b <= i
//
// sel_acc
//   00    acc_next <= x
//   01    acc_next <= sig_r
//   10    acc_next <= i
//   11    acc_next <= acc_reg
//   
//////////////////////////////////////////////////////////////////////////////////

module alu_accumulator(
    input wire [7:0] x,
    input wire [7:0] i,
    input wire sel_b,
	 input wire sel_flags,
    input wire [1:0] sel_acc,
    input wire [2:0] opcode,
    input wire rst,
    input wire clk,
    output reg [3:0] flags,
    output reg [7:0] acc
    );

   // signals
   reg [7:0] acc_reg;
   reg [7:0] acc_next;
	reg [3:0] flags_reg;
   reg [3:0] flags_next;
	wire [7:0] sig_b;
   wire [7:0] sig_r;
	wire [3:0] sig_flags;
   
   // mux entrada b de la alu
   assign sig_b = sel_b ? i : x;
   
   // instancia de la alu
   alu inst_alu (
       .a(acc_reg), 
       .b(sig_b), 
       .cin(flags_reg[0]), 
       .opcode(opcode), 
       .r(sig_r), 
       .flags(sig_flags)
       );
	
   // mux accumulator
   always @(*) begin
      // salida del module
      acc = acc_reg;
      flags = flags_reg;
      
      case(sel_acc)
         2'b00: begin
            acc_next = x;
         end
         2'b01: begin
            acc_next = acc_reg;
         end
         2'b10: begin
            acc_next = i;
         end
         default: begin
            acc_next = sig_r;
         end
      endcase
   end
   
	// registro del acumulador y flags
   always @(posedge rst,posedge clk) begin
      if(rst) begin
         acc_reg <= 8'h00;
			flags_reg <= 4'h0;
		end
      else begin
         acc_reg <= acc_next;
			flags_reg <= flags_next;
		end
   end
	
	always @(*) begin
		if(sel_flags)
			flags_next = sig_flags;
		else
			flags_next = flags_reg;
	end
	
endmodule

