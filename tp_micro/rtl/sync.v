//////////////////////////////////////////////////////////////////////////////////
// SYNC
// Modulo encargado de syncronizar el rst
//////////////////////////////////////////////////////////////////////////////////

module sync(
    input wire arst,
    input wire clk,
    output reg [0:0] sync
    );
	 
	 reg [1:0] sig;
	 
	 always@(posedge clk, posedge arst) begin
		if(arst)
			sig <= 2'b11;
		else begin
			sig[1] <= sig[0];
			sig[0] <= 1'b0;
		end
	 end
	 
	 always@(*) begin
		sync = arst | sig[1];
	 end
	 
endmodule

