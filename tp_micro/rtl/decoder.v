//////////////////////////////////////////////////////////////////////////////////
// Instrucciones:
//
// Instrucciones de movimiento de datos
// LOAD     A,X     00 00 00 00     ---> usa memoria
// STORE    A,X     00 00 00 01     ---> usa memoria para escribir
// LOADI    A,I     00 00 00 10
// STOREI   A,I     00 00 00 11     ---> usa memoria para escribir
//
// Instrucciones aritmeticas
// ADD      A,X     01 00 00 00     ---> usa memoria
// SUB      A,X     01 00 00 01     ---> usa memoria
// ADDC     A,X     01 00 00 10     ---> usa memoria
// SUBC     A,X     01 00 00 11     ---> usa memoria
//
// ADDI     A,I     01 00 01 00
// SUBI     A,I     01 00 01 01
// ADDIC    A,I     01 00 01 10
// SUBIC    A,I     01 00 01 11
//
// Instrucciones logicas
// NOR      A,X     10 00 00 00     ---> usa memoria
// NAND     A,X     10 00 00 01     ---> usa memoria
// XOR      A,X     10 00 00 10     ---> usa memoria
// XNOR     A,X     10 00 00 11     ---> usa memoria
//
// NORI     A,I     10 00 01 00
// NANDI    A,I     10 00 01 01
// XORI     A,I     10 00 01 10
// XNORI    A,I     10 00 01 11
//
// Instrucciones de salto
// JUMP     I       11 00 00 00
// JZ       I       11 00 00 01
// JC       I       11 00 00 10
// JN       I       11 00 00 11
// 
// opcode
//		000	ADD
//		001	SUB
//		010	ADDC
//		011	SUBC
//		100	NOR
//		101	NAND
//		110	XOR
//		111	XNOR
//
// flags
//		flags[0]		cout
//		flags[1]		zero
//		flags[2]		negative
//		flags[3]		ov
// 
// en_pc
//    en_pc[0]    jump
//    en_pc[1]    x
//
// sel_read
//    1           read
//    0           write
//////////////////////////////////////////////////////////////////////////////////

module decoder(
    input wire [7:0] inst,          // FROM UC
    input wire [3:0] flags,         // FROM ALU_ACCUMULATOR
    output wire to_memory,          // UC
    output wire sel_dout_ram,       // UC
    output wire sel_add_ram,        // UC
    output wire sel_rw,             // UC
	 output wire sel_flags,				// ALU_ACCUMULATOR
    output wire sel_b,              // ALU_ACCUMULATOR
    output wire [2:0] opcode,       // ALU_ACCUMULATOR
    output reg [1:0] sel_acc,       // ALU_ACCUMULATOR
    output reg en_pc                // PC
    );
   
   //////////////////////////////////////////////////////////////////////////////////
   // PC
   // enable [x:jump]
   always@(*) begin
      en_pc = 1'b0;
      if(inst[7:6] == 2'b11)
         if((inst[1:0] == 2'b00) ||                           // JMP
            ((inst[1:0] == 2'b01) && (flags[1] == 1)) ||      // JZ
            ((inst[1:0] == 2'b10) && (flags[0] == 1)) ||      // JC
            ((inst[1:0] == 2'b11) && (flags[2] == 1)))        // JN
            en_pc = 1'b1;
   end
   
   //////////////////////////////////////////////////////////////////////////////////
   // ALU_ACUMULATOR
   // control mux b entrada alu
   assign sel_b = inst[2];
   
   // control alu
   assign opcode[2:0] = {inst[7],inst[1:0]};
   
   // control accumulator
   always@(*) begin
      if((inst[7:6] == 2'b01) || (inst[7:6] == 2'b10))
         sel_acc = 2'b11;     // r
      else if((inst[7:6] == 2'b00) && (inst[1:0] != 2'b01) && (inst[1:0] != 2'b11))
         sel_acc = inst[1:0]; // x o i
      else
         sel_acc = 2'b01;     // retencion
   end
	
	// control registro flags
	assign sel_flags = ((inst[7:6] == 2'b01) || (inst[7:6] == 2'b10)) ? 1'b1 : 1'b0;
   
   //////////////////////////////////////////////////////////////////////////////////
   // UC
   // control mux dout_ram
   assign sel_dout_ram = inst[1];
   
   // control mux nrw
   assign sel_rw = ((inst[7:6] == 2'b00) && (inst[0] == 1'b1)) ? 1'b1 : 1'b0;
   
   // control mux add_ram
   assign sel_add_ram = ((inst[7:6] == 2'b00) && (inst[1:0] == 2'b11)) ? 1'b1 : 1'b0;
   
   // to memory flag
   assign to_memory = ((inst[7:6] != 2'b11) && (!inst[2]) && (inst != 8'h02)) ? ~inst[2] : inst[2];
      
endmodule

