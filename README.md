- - -

Workspace de ejercicios/proyectos descriptos en Verilog
====================

- - -

## UART

Este proyecto consiste en el diseño de un circuito receptor y transmisor *UART* con trama 8N1.

Dentro de este directorio se puede encontrar:

+ Proyecto ISE
+ Código fuente
+ Testbench


## TP MICRO

Proyecto integrador de la materia *Diseño de Circuitos Integrados Digitales* de la carrera *Ingenieria Electrónica* dictada en la Universidad Tecnológica Nacional (UTN) Facultad Regional Buenos Aires (FRBA) año 2017.

El mismo consiste en el desarrollo de un microcontrolador básico de 8 bits y 24 instrucciones. Siguiendo la estructura FETCH-DECODE-EXECUTE, agrega un cuarto estado de demora en el caso de que se deba acceder a la memoria externa.

Dentro de este directorio se puede encontrar:

+ Código fuente
+ Testbench
+ Assembler de cada test provistos por la cátedra


## DCID práctica

Incluye los ejercicios propuestos para la materia *Diseño de Circuitos Integrados Digitales* de la carrera *Ingenieria Electrónica* dictada en la Universidad Tecnológica Nacional (UTN) Facultad Regional Buenos Aires (FRBA) año 2017.

Dentro de este directorio se puede encontrar:

+ Proyecto ISE
+ Código fuente
+ Testbench


## EAMTA Lab 1

Ejercicio realizado durante el curso *Diseño Avanzado de Circuitos Digitales* dentro de EAMTA 2017.

El mismo consiste en el diseño de circuitos básicos de un microcontrolador.

Dentro de este directorio se puede encontrar:

+ Proyecto ISE
+ Código fuente
+ Testbench




***

Contacto:

+ agustin.diazantuna@gmail.com

***
