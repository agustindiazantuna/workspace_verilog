//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 13 - Implementar en Verilog un registro de desplazamiento de N bits 
// de largo.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio13#(parameter N = 4)(
	 input wire sdin,
	 input wire rst,
	 input wire clk,
	 output reg sdout
    );
	 
	 reg [N-1 : 0] aux;
	 
	 // Tener en cuenta que la asignacion es de reg a reg
	 always@(*) begin
		sdout = aux[N-1];
	 end
	 
	 always@(posedge clk, posedge rst) begin
		if(rst)
			aux <= {N{1'b0}};
		else
			aux <= {aux[N-2 : 0],sdin};
	 end
	 	 
endmodule
