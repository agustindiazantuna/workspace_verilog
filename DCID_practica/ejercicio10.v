//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 10 - Implementar en Verilog un contador genérico de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio10 #(parameter N = 4)(
	 input wire rst,
	 input wire srst,
	 input wire clk,
	 output reg [N-1 : 0] cuenta
    );
	 
	 always @(posedge clk, posedge rst) begin
		if(rst)
			cuenta <= {N,1'b0};
		else
			if (srst)
				cuenta <= {N,1'b0};
			else
				cuenta <= cuenta + {{(N-1){1'b0}},1'b1};
	 end
	 
endmodule
