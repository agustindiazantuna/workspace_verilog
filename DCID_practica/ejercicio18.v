//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 18 - Implementar en Verilog el circuito anterior pero con 
// codificación one hot.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio18(
	 input wire [1:0] x,
	 input wire rst,
	 input wire clk,
	 output reg [2:0] y
    );
	 
	 localparam sA = 5'b00001;
	 localparam sB = 5'b00010;
	 localparam sC = 5'b00100;
	 localparam sD = 5'b01000;
	 localparam sE = 5'b10000;
	 
	 reg [4 : 0] state;
	 
	 always@(*) begin
		case(state)
			sA: begin
				y = 3'b001;
			end
			sB: begin
				y = 3'b011;
			end
			sC: begin
				y = 3'b101;
			end
			sD: begin
				y = 3'b100;
			end
			default: begin // sE
				y = 3'b010;
			end
		endcase
	 end
	 
	 always@(posedge clk, posedge rst) begin
		if(rst)
			state <= sA;
		else begin
			case(state)
				sA: begin
					y <= 3'b001;
					if(x == 2'b11)
						state <= sB;
					else if(x == 2'b00)
						state <= sE;
				end
				sB: begin
					y <= 3'b011;
					if(x == 2'b10)
						state <= sA;
				end
				sC: begin
					y <= 3'b101;
					if(x == 2'b10)
						state <= sD;
					else if(x == 2'b11)
						state <= sA;
				end
				sD: begin
					y <= 3'b100;
					if(x == 2'b11)
						state <= sB;
				end
				default: begin // sE
					y <= 3'b010;
					if(x == 2'b11)
						state <= sC;
					else if(x == 2'b10)
						state <= sB;
				end
			endcase
		end
	 end
	 
endmodule
