//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 8 - Implementar en Verilog un flip flop D con las siguientes entradas:
// - Reset asincrónico
// - Reset sincrónico
// - Set asincrónico
// - Set sincrónico
//////////////////////////////////////////////////////////////////////////////////

module ejercicio08(
	 input wire d,
	 input wire rst,
	 input wire srst,
	 input wire set,
	 input wire sset,
	 input wire clk,
	 output reg q
    );

	always @(posedge clk, posedge rst) begin
		if(rst)
			q <= 1'b0;
		else if(set)
			q <= 1'b1;
		else begin
			if(srst)
				q <= 1'b0;
			else if(sset)
				q <= 1'b1;
			else
				q <= d;
			end
	end

endmodule
