//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 4 - Implementar en Verilog un sumador genérico de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio04#(parameter N = 4)(
	 input wire [N-1 : 0] x0,
	 input wire [N-1 : 0] x1,
	 input wire cin,
	 output wire cout,
	 output wire [N-1 : 0] y
    );
	
	wire [N : 0] i;
	
	assign i = x0 + x1 + cin;
	assign y = i[N-1 : 0];
	assign cout = i[N : N];
	
endmodule
