`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 9 - Implementar en Verilog un registro de N bits con las siguientes entradas:
// - Reset asincrónico
// - Reset sincrónico
// - Set asincrónico
// - Set sincrónico
//////////////////////////////////////////////////////////////////////////////////
module ejercicio09 #(parameter N = 4)(
	 input wire [N-1 : 0] d,
	 input wire rst,
	 input wire srst,
	 input wire set,
	 input wire sset,
	 input wire clk,
	 output reg [N-1 : 0] q
    );
	 
	 always @(posedge clk, posedge rst) begin
		if(rst)
			q <= {(N){1'b0}};
		else if(set)
			q <= {(N){1'b1}};
		else begin
			if (srst)
				q <= {(N){1'b0}};
			else if(sset)
				q <= {(N){1'b1}};
			else
				q <= d;
			end
	 end
	 
endmodule
