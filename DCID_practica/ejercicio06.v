//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 6 - Implementar en Verilog un multiplicador sin signo genérico de
// N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio06 #(parameter N = 4)(
	 input wire [N-1 : 0] x0,
	 input wire [N-1 : 0] x1,
	 input wire [(2*N)-1 : 0] y
    );

	assign y = {{N{1'b0}},x0} * {{N{1'b0}},x1};

endmodule
