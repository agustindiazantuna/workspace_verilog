//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 7 - Implementar en Verilog un multiplicador en complemento a dos
// genérico de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio07 #(parameter N = 4)(
	 input wire [N-1 : 0] x0,
	 input wire [N-1 : 0] x1,
	 output wire [(2*N)-1 : 0] y
    );

	assign y = {{N{x0[N-1]}},x0} * {{N{x1[N-1]}},x1};

endmodule
