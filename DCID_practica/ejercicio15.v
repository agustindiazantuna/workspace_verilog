//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 15 - Implementar en Verilog un registro de desplazamiento genérico 
// de N bits y señal de enable. Cuando dicha señal de enable sea '0', el contenido
// del registro no deberá ser modificado.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio15#(parameter N = 4)(
	 input wire sdin,
	 input wire enable,
	 input wire rst,
	 input wire srst,
	 input wire clk,
	 output reg sdout
    );
	 
	 reg [N-1 : 0] aux;
	 
	 always@(*) begin		// asignacion reg to reg
		sdout = aux[N-1];
	 end
	 	 
	 always@(posedge clk, posedge rst) begin
		if(rst)
			aux <= {N{1'b0}};
		else if(srst)
			aux <= {N{1'b0}};
		else if(enable)
			aux <= {aux[N-2 : 0],sdin};		
	 end
	 
endmodule
