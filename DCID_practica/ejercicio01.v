//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 1 - Implementar en Verilog las compuertas: AND, OR, XOR, AOI, OAI.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio01(
    input wire a,
    input wire b,
    input wire c,
    input wire d,
    output wire sand,
    output wire sor,
    output wire sxor,
    output wire saoi,
    output wire soai
    );

	assign sand = a & b;
	assign sor = a | b;
	assign sxor = a ^ b;
	assign saoi = ~((a & b) | (c & d));
	assign soai = ~((a | b) & (c | d));

endmodule
