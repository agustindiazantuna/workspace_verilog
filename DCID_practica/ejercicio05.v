//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 5 - Implementar en Verilog un restador genérico de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio05 #(parameter N = 4)(
	 input wire [N-1 : 0] x0,
	 input wire [N-1 : 0] x1,
	 input wire bin,
	 output wire bout,
	 output wire [N-1 : 0] y
    );

	wire [N : 0] i;
	
	// ver
	assign i = {x0,1'b0} - {x1,1'b0} - bin; // {{(N-1){1'b0}},bin}
	assign bout = 1'b0;

endmodule
