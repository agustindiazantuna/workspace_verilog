//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 11 - Implementar en Verilog un contador genérico de N bits y señal 
// de carga. Cuando dicha señal de carga sea '1', el valor de la cuenta pasará a 
// ser el valor de una señal de entrada de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio11 #(parameter N = 4)(
	 input wire [N-1 : 0] pdata,
	 input wire pl,
	 input wire rst,
	 input wire srst,
	 input wire clk,
	 output reg [N-1 : 0] count
    );
	 
	 always @(posedge clk, posedge rst) begin
		if(rst)
			count <= {N{1'b0}};
		else if(srst)
			count <= {N{1'b0}};
		else if(pl)
			count <= pdata;
		else
			count <= count + {{(N-1){1'b0}},1'b1};
	 end
	 
endmodule
