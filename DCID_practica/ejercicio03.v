//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 3 - Implementar en Verilog un multiplexor de 4 lineas de selección
// y 16 entradas de datos. Cada entrada de datos debe ser de un ancho de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio03 #(parameter N = 8)(
	 input wire [N-1 : 0] x0,
	 input wire [N-1 : 0] x1,
	 input wire [N-1 : 0] x2,
	 input wire [N-1 : 0] x3,
	 input wire [N-1 : 0] x4,
	 input wire [N-1 : 0] x5,
	 input wire [N-1 : 0] x6,
	 input wire [N-1 : 0] x7,
	 input wire [N-1 : 0] x8,
	 input wire [N-1 : 0] x9,
	 input wire [N-1 : 0] x10,
	 input wire [N-1 : 0] x11,
	 input wire [N-1 : 0] x12,
	 input wire [N-1 : 0] x13,
	 input wire [N-1 : 0] x14,
	 input wire [N-1 : 0] x15,
	 input wire [3 : 0] sel,
	 output reg [N-1 : 0] y
    );
	 
	 always@(*) begin
		case(sel)
			4'b0000 :
					y = x0;
			4'b0001 :
					y = x1;
			4'b0010 :
					y = x2;
			4'b0011 :
					y = x3;
			4'b0100 :
					y = x4;
			4'b0101 :
					y = x5;
			4'b0110 :
					y = x6;
			4'b0111 :
					y = x7;
			4'b1000 :
					y = x8;
			4'b1001 :
					y = x9;
			4'b1010 :
					y = x10;
			4'b1011 :
					y = x11;
			4'b1100 :
					y = x12;
			4'b1101 :
					y = x13;
			4'b1110 :
					y = x14;
			default :
					y = x15;
			endcase
	 end
		
endmodule
