//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 14 - Implementar en Verilog un registro de desplazamiento genérico 
// de N bits y señal de carga. Cuando dicha señal de carga sea '1', el contenido 
// del registro pasará a ser el valor de una señal de entrada de N bits.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio14#(parameter N = 4)(
	 input wire sdin,
	 input wire pl,
	 input wire [N-1 : 0] pdata,
	 input wire rst,
	 input wire srst,
	 input wire clk,
	 output reg dout
    );
	 
	 reg [N-1 : 0] aux;
	 
	 always@(*) begin
		dout = aux[N-1];
	 end
	 
	 always@(posedge clk, posedge rst) begin		
		if(rst)
			aux <= {N{1'b0}};
		else if(srst)
			aux <= {N{1'b0}};
		else if(pl)
			aux <= pdata;
		else
			aux <= {aux[N-2 : 0],sdin};
	 end
	 
endmodule
