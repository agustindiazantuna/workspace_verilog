//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 19 - Implementar en Verilog un detector de flancos. Este circuito 
// tendrá dos salidas denominadas rising y falling las cuales serán '1'cuando en 
// la señal de entrada se produzca un flanco de subida o de bajada respectivamente.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio19(
    input wire x,
	 input wire rst,
	 input wire clk,
	 output reg y
	 );
	
	localparam sA = 3'b000;
	localparam sB = 3'b001;
	localparam sC = 3'b010;
	localparam sD = 3'b011;
	localparam sE = 3'b100;
	
	reg [2:0] state;

	always@(*) begin
		case(state)
			sC: begin
				y = 1'b1;
			end
			sE: begin
				y = 1'b1;
			end
			default: begin
				y = 1'b0;
			end
		endcase
	end
	
	always@(posedge clk, posedge rst) begin
		if(rst)
			state <= sA;
		else begin
			case(state)
				sA: begin
					if(!x)
						state <= sB;
					else
						state <= sC;
				end
				sB: begin
					if(x)
						state <= sD;
				end
				sC: begin
					if(!x)
						state <= sE;
				end
				sD: begin
					if(x)
						state <= sC;
					else
						state <= sE;
				end
				default: begin
					if(!x)
						state <= sB;
					else
						state <= sD;
				end
			endcase
		end
	end
	
endmodule
