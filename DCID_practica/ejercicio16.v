//////////////////////////////////////////////////////////////////////////////////
//Ejercicio 16 - Implementar en Verilog un circuito que detecte la secuencia 
// "0010110" de una señal de entrada. Deberán relizarse dos implementaciones 
// diferentes: mediante una FSM y mediante un registro de desplazamiento y la 
// lógica de detección adecuada.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio16(
	 input wire x,
	 input wire rst,
	 input wire clk,
	 output reg y
    );
	 
//	 //	Implementacion con SHIFT REGISTER
//	 reg [6 : 0] aux;
//
//	 always@(*) begin
//	 	y = (aux == 7'b0010110) ? 1'b1 : 1'b0;
//	 end
//	 
//	 always@(posedge clk, posedge rst) begin
//		if(rst)
//			aux <= {7{1'b0}};
//		else
//			aux <= {aux[5 : 0],x};		
//	 end
	 
	 //	Implementacion con FSM
	 
	 reg [2 : 0] state;
	 
	 localparam s0 = 3'b000;
	 localparam s1 = 3'b001;
	 localparam s2 = 3'b010;
	 localparam s3 = 3'b011;
	 localparam s4 = 3'b100;
	 localparam s5 = 3'b101;
	 localparam s6 = 3'b110;
	 localparam s7 = 3'b111;

	 always@(*) begin
		y = (state == s7) ? 1'b1 : 1'b0;
	 end
	 
	 always@(posedge clk, posedge rst) begin		
		if(rst)
			state <= s0;
		else begin
			case(state)
				s0: begin
					if(!x)
						state <= s1;
				end
				s1: begin
					if(!x)
						state <= s2;
					else
						state <= s0;
				end
				s2: begin
					if(x)
						state <= s3;
				end
				s3: begin
					if(!x)
						state <= s4;
					else
						state <= s0;
				end
				s4: begin
					if(x)
						state <= s5;
					else
						state <= s2;
				end
				s5: begin
					if(x)
						state <= s6;
					else
						state <= s1;
				end
				s6: begin
					if(!x)
						state <= s7;
					else
						state <= s1;
				end
				default: begin	// s7
					if(x)
						state <= s0;
					else
						state <= s2;
				end
			endcase
		end
	 end
	 
endmodule
