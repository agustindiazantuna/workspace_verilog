//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 17 - Implementar en Verilog el circuito de Moore correspondiente al 
// siguiente diagrama de estados:
//////////////////////////////////////////////////////////////////////////////////

module ejercicio17(
	 input wire [1:0] x,
	 input wire rst,
	 input wire clk,
	 output reg [2:0] y
    );
	 
	 localparam sA = 3'b000;
	 localparam sB = 3'b001;
	 localparam sC = 3'b010;
	 localparam sD = 3'b011;
	 localparam sE = 3'b100;
	 
	 reg [2 : 0] state;
	 
	 always@(*) begin
		case(state)
			sA: begin
				y = 3'b001;
			end
			sB: begin
				y = 3'b011;
			end
			sC: begin
				y = 3'b101;
			end
			sD: begin
				y = 3'b100;
			end
			default: begin // sE
				y = 3'b010;
			end
		endcase
	 end
	 
	 always@(posedge clk, posedge rst) begin
		if(rst)
			state <= sA;
		else begin
			case(state)
				sA: begin
					if(x == 2'b11)
						state <= sB;
					else if(x == 2'b00)
						state <= sE;
				end
				sB: begin
					if(x == 2'b10)
						state <= sA;
				end
				sC: begin
					if(x == 2'b10)
						state <= sD;
					else if(x == 2'b11)
						state <= sA;
				end
				sD: begin
					if(x == 2'b11)
						state <= sB;
				end
				default: begin // sE
					if(x == 2'b11)
						state <= sC;
					else if(x == 2'b10)
						state <= sB;
				end
			endcase
		end
	 end
	 
endmodule
