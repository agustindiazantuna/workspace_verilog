//////////////////////////////////////////////////////////////////////////////////
// Ejercicio 12 - Implementar en Verilog un contador genérico de N bits y señal 
// de enable. Cuando dicha señal de enable sea '0', el valor de la cuenta no 
// deberá ser modificado.
//////////////////////////////////////////////////////////////////////////////////

module ejercicio12 #(parameter N = 4)(
	 input wire rst,
	 input wire srst,
	 input wire enable,
	 input wire clk,
	 output reg [N-1 : 0] count
    );
	 
	 always@(posedge clk, posedge rst) begin
		if(rst)
			count <= {N{1'b0}};
		else if(srst)
			count <= {N{1'b0}};
		else if(enable)
			count <= count + {{(N-1){1'b0}},1'b1};
	 end
	 
endmodule
